
//converts marginRight to margin-right
export default function convertStrtoIfen(str){
  
  const arr=str.split('');
  
  const newArr=[];
  
  
  arr.forEach((char)=>{
    
    if(char.toUpperCase()===char){
      newArr.push('-');
      newArr.push(char.toLowerCase())
    }
    
    else newArr.push(char)
    
       
  })
 
  
  return newArr.join('');
  
  

  
}