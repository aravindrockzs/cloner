import { useEffect, useState, useRef } from 'react'

import getclonerWebComponentClass from '../web-components/clonerShadow';

import styles from "./Home.module.css"
import Inspector from "./Inspector";

import { applyStateToDOM } from '../helpers/setDomstate';


import classNames from 'classnames/bind';

const cx = classNames.bind(styles)





export default function Home() {

  const [html, sethtml] = useState("Loading")


  const devicesArr = [
    {
      name: '4k',
      height: 1900,
      width: 2560,
    },

    {
      name: 'Laptop L',
      height: 1700,
      width: 1440,
    },
    {
      name: 'Laptop S',
      height: 1500,
      width: 1024,
    },
    {
      name: 'Tab',
      height: 900,
      width: 768,
    },
    {
      name: 'Phone',
      height: 768,
      width: 415,

    },
    {
      name: "Full Screen",
      width: '100%',
      height: '100%'
    }
  ]

  const [currentDevice, setCurrentDevice] = useState(devicesArr[devicesArr.length - 1],)



  // const [idocument,setIdocument] = useState();


  const idocument = useRef();
  const iwindow = useRef();

  const iobserver = useRef();



  const selectorState = useRef();
  const currentElement = useRef();
  const icloner = useRef();

  //for differentiating click vs double click

  const timerRef = useRef(0);
  const delayRef = useRef(200);
  const preventRef = useRef(false);




  const selectedElement = useRef(null)




  function isElementVisible(el, document) {
    var rect = el.getBoundingClientRect(),
      vWidth = window.innerWidth || document.documentElement.clientWidth,
      vHeight = window.innerHeight || document.documentElement.clientHeight,
      efp = function (x, y) { return document.elementFromPoint(x, y) };

    // Return false if it's not in the viewport
    if (rect.right < 0 || rect.bottom < 0
      || rect.left > vWidth || rect.top > vHeight)
      return false;

    // Return true if any of its four corners are visible
    return (
      el.contains(efp(rect.left, rect.top))
      || el.contains(efp(rect.right, rect.top))
      || el.contains(efp(rect.right, rect.bottom))
      || el.contains(efp(rect.left, rect.bottom))
    );
  }

  const SetShadowBox = (e, frameDocument, type) => {
    // console.log(isElementVisible(e.target, frameDocument))

    const { x, y, height, width } = e.target.getBoundingClientRect()
    const cloner = frameDocument.getElementsByTagName('cloner-shadow')[0]

    cloner.setAttribute('x', x);
    cloner.setAttribute('y', y);
    cloner.setAttribute('height', height);
    cloner.setAttribute('width', width);


    if (type === "scroll" && selectorState.current === "click") {
      cloner.setAttribute('type', "click");
      return;
    }

    if (type === "scroll" && selectorState.current === "dblclick") {
      cloner.setAttribute('type', "dblclick");
      return;
    }
    cloner.setAttribute('type', type);

  }


  useEffect(() => {

    document.getElementById('myframe').onload = function () {


      const frameDocument = document.getElementById('myframe').contentWindow.document

      const frameWindow = document.getElementById('myframe').contentWindow

      applyStateToDOM(frameDocument)


      idocument.current = frameDocument;
      iwindow.current = frameWindow;


      document.getElementById('myframe').contentWindow.customElements.define(
        'cloner-shadow', getclonerWebComponentClass(frameWindow, currentElement, reset));


      const cloner = document.createElement('cloner-shadow')
      cloner.setAttribute('type', 'hover')
      cloner.setAttribute('x', 0);
      cloner.setAttribute('y', 0);
      cloner.setAttribute('image', "false")
      cloner.setAttribute('canvasw', currentDevice.width)
      cloner.setAttribute('canvash', currentDevice.height)



      icloner.current = cloner



      frameDocument.body.prepend(cloner);

      // setting the initail  selectorState
      selectorState.current = 'hover'




      frameDocument.addEventListener("scroll", (e) => eventHandler(e, 'scroll'))

      frameDocument.addEventListener('click', (e) => eventHandler(e, 'click'))


      frameDocument.addEventListener('mouseover', (e) => eventHandler(e, 'hover'))

      frameDocument.addEventListener('dblclick', (e) => eventHandler(e, 'dblclick'))



      // frameDocument.addEventListener('mouseout', (e) => {
      //   clearMasks()
      // })



    };
  }, [html])

  function reset() {

    selectorState.current = 'hover';
    icloner.current.setAttribute('image', "false")
    currentElement.current = '';
    icloner.current.setAttribute('type', 'initial')
  }



  const eventHandler = (event, type) => {


    if (type === "hover") {
      if (selectorState.current === 'click' || selectorState.current === 'dblclick') return;
      SetShadowBox(event, idocument.current, type)
      selectorState.current = 'hover';
      return;
    }


    if (type === 'click') {
      if (event.target === icloner.current) {
        return;
      }
      event.preventDefault();


      timerRef.current = setTimeout(function () {
        if (!preventRef.current) {







          if (currentElement.current && currentElement.current !== event.target) {
            // console.log('different element');
            currentElement.current.setAttribute('contenteditable', false);
            selectorState.current = 'hover';
            currentElement.current = '';
            icloner.current.setAttribute('type', 'initial')
            icloner.current.setAttribute('image', "false")
            return;
          }

          if (selectorState.current === 'dblclick') return;




          selectedElement.current = event
          currentElement.current = event.target;

          SetShadowBox(selectedElement.current, idocument.current, type)
          selectorState.current = 'click';
          return;
        }
        preventRef.current = false;
      }, delayRef.current);



    }

    if (type === "scroll") {
      if (selectorState.current === 'click') {

        if (selectedElement.current) {
          SetShadowBox(selectedElement.current, idocument.current, type)
        }
      }

      if (selectorState.current === 'dblclick') {

        if (selectedElement.current) {
          SetShadowBox(selectedElement.current, idocument.current, type)
        }

      }

    }

    if (type === 'dblclick') {
      event.preventDefault();

      clearTimeout(timerRef.current)
      preventRef.current = true;



      if (currentElement.current && currentElement.current !== event.target) {
        // console.log('different element');

        currentElement.current.setAttribute('contenteditable', false);
        selectorState.current = 'hover';
        icloner.current.setAttribute('image', false)
        currentElement.current = '';
        icloner.current.setAttribute('type', 'initial')
        return;
      }



      if (event.target.nodeName === "IMG") {

        icloner.current.setAttribute("image", "true");


        selectedElement.current = event
        currentElement.current = event.target;

        SetShadowBox(selectedElement.current, idocument.current, type)
        selectorState.current = 'dblclick';
        return;




      }










      if (event.target.firstChild?.nodeType === Node.TEXT_NODE) {

        event.target.setAttribute('contenteditable', 'true');

        const textNodeLength = event.target.textContent.length;
        console.log("text", textNodeLength)


        let range = new Range();

        var sel = iwindow.current.getSelection()

        range.setStart(event.target.firstChild, textNodeLength)
        range.collapse(true)

        sel.removeAllRanges()
        sel.addRange(range)

        iobserver.current = new MutationObserver(mutationRecords => {
          // console.log(mutationRecords); 
          SetShadowBox(selectedElement.current, idocument.current, type)
        });

        iobserver.current.observe(event.target, {
          childList: true, // observe direct children
          subtree: true, // and lower descendants too
          characterDataOldValue: true // pass old data to callback
        });

        selectedElement.current = event
        currentElement.current = event.target;

        SetShadowBox(selectedElement.current, idocument.current, type)
        selectorState.current = 'dblclick';
        return;

      }





    }




  }




  useEffect(() => {

    fetch('https://d2adrmzxvukca6.cloudfront.net/Disney%2B+Hotstar+-+Watch+TV+Shows%2C+Movies%2C+Specials%2C+Live+Cricket+%26+Football+(1).html')
      .then(function (response) {
        return response.text()
      })
      .then(function (html) {

        sethtml(html)
      })
      .catch(function (err) {
        console.log('Failed to fetch page: ', err);
      });


  }, [])

  const clickHandler = (e) => {

    const deviceType = e.target.id;

    const iframe = document.getElementById('myframe');

    const newDevice = devicesArr.find(deviceObj => {
      return deviceObj.name === deviceType
    })
    console.log("object,", newDevice);
    const scaleFactor = newDevice.width > window.innerWidth - 250 ? (window.innerWidth - 250) / newDevice.width : 1;
    console.log(newDevice.width * scaleFactor);
    let deviceWidthCenter = (newDevice.width * scaleFactor) / 2
    let AvailCenter = (window.innerWidth - 250) / 2
    console.log(AvailCenter, deviceWidthCenter);
    iframe.style.setProperty('transform-origin', '0 0')
    iframe.style.setProperty('transform', `translate(${AvailCenter - deviceWidthCenter}px, 0px)`)

    if (newDevice.width > window.innerWidth - 250) {


      iframe.style.setProperty('transform', `  scale(${scaleFactor}) `)

    }

    icloner.setAttribute('canvasw', newDevice.width)
    icloner.setAttribute('canvash', newDevice.height)






    setCurrentDevice(newDevice)
  }


  //adding Hover border using only js

  function applyMask(target, clicked = false) {

    // if(clicked){
    //   createMask(target,clicked);
    //   return
    // }
    if (idocument.current.getElementsByClassName('highlight-wrap').length > 0) {
      resizeMask(target);
    } else {
      createMask(target);
    }


  }

  function resizeMask(target) {
    var rect = target.getBoundingClientRect();
    var SelectedElementObj = idocument.current.getElementsByClassName('highlight-wrap')[0];
    SelectedElementObj.style.top = rect.top + "px";
    SelectedElementObj.style.width = rect.width + "px";
    SelectedElementObj.style.height = rect.height + "px";
    SelectedElementObj.style.left = rect.left + "px";
    //SelectedElementObj.style.WebkitTransition='top 0.2s';
  }

  function createMask(target, clicked = false) {
    var rect = target.getBoundingClientRect();
    var SelectedElementObj = idocument.current.createElement("div");
    SelectedElementObj.className = clicked ? "highlight-box" : 'highlight-wrap';
    SelectedElementObj.style.position = 'fixed';
    SelectedElementObj.style.top = rect.top + "px";
    SelectedElementObj.style.width = rect.width + "px";
    SelectedElementObj.style.height = rect.height + "px";
    SelectedElementObj.style.left = rect.left + "px";
    SelectedElementObj.style.backgroundColor = "rgba(80, 86, 241, 0.08)";
    SelectedElementObj.style.border = '2px solid #5056f1';
    SelectedElementObj.style.borderRadius = '5px';
    SelectedElementObj.style.cursor = 'default';
    SelectedElementObj.style.pointerEvents = 'none';
    SelectedElementObj.style.zIndex = "99999999999999";
    idocument.current.body.appendChild(SelectedElementObj);

  }

  function clearAddedDivs(appliedClass) {


    var hwrappersLength = idocument.current.getElementsByClassName(`${appliedClass}`).length;
    var hwrappers = idocument.current.getElementsByClassName(`${appliedClass}`);
    if (hwrappersLength > 0) {
      for (var i = 0; i < hwrappersLength; i++) {
        // console.log("Removing existing wrap");
        hwrappers[i].remove();
      }
    }
  }

  function clearMasks() {
    clearAddedDivs('highlight-wrap')
  }

  function clearBox() {
    clearAddedDivs('highlight-box')
  }
  //----------------------------------------------------------------




  return (
    <div id="home" className={cx('home_container')}>


      <iframe id='myframe' height={currentDevice.height} width={currentDevice.width}
        className={cx('myframe')} srcDoc={html} title='title' />

      <div onClick={clickHandler} className={cx('responsive-tab')}>
        <div id='4k' className={cx('four_k')}>
          4k
        </div>

        <div id='Laptop L' className={cx('laptop_L')}>
          Ld
        </div>

        <div id='Laptop S' className={cx('laptop_L')}>
          Laptop S
        </div>

        <div id='Tab' className={cx('laptop_L')}>
          Tab
        </div>

        <div id='Phone' className={cx('laptop_L')}>
          Phone
        </div>
      </div>

      <Inspector />
    </div>
  )
}