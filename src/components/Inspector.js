import React, {useEffect, useState} from 'react'
import { v4 as uuidv4 } from 'uuid';

import { localStorageSet ,localStorageGet } from '../helpers/localstorage'
import convertStrtoIfen  from '../helpers/toIfen'

import className from 'classnames/bind'
import styles from './Inspector.module.css'

const cx = className.bind(styles)



const Inspector = ({clickedEl}) => {

	 


	 if(localStorageGet("DOMState") === null){
			localStorageSet("DOMState",[]);
	 }

   const [openInspector, setInspector] = useState(true)

	 const cssProperties = ['marginRight', 'marginTop', 'marginLeft', 'marginBottom',
	  'paddingTop', 'paddingBottom', 'paddingLeft', 'paddingRight']

		const initialState={
			elementRef: "",
			elementProperties:{}

		}

		cssProperties.forEach(property =>{
         initialState.elementProperties[property] = {
					 prev:"",
					 current:""
				 }
		})

	 const [elementCss, setElementCss]= useState(initialState)

	 const setMarginPadding =(cssPropertiesObj)=>{

		const clickedDOMState ={
			...elementCss,
			elementRef: createQuerySelector(clickedEl)
		}


		const elementProperties = {}

		cssProperties.forEach(property =>{
         elementProperties[property] = {
					 prev:cssPropertiesObj[property],
					 current:cssPropertiesObj[property]
				 }
		})

		clickedDOMState.elementProperties= elementProperties;


		setElementCss(clickedDOMState);

	 }

	 function createQuerySelector(element) {
		if (element.id) {
				return `#${element.id}`;
		}
	
		const path = [];
		let currentElement = element;
		let error = false;
	
		while (currentElement.tagName !== 'BODY') {
				const parent = currentElement.parentElement;
	
				if (!parent) {
						error = true;
						break;
				}
	
				const childTagCount= {};
				let nthChildFound = false;
	
				for (const child of parent.children) {
						const tag = child.tagName;
						const count = childTagCount[tag] || 0;
						childTagCount[tag] = count + 1;
	
						if (child === currentElement) {
								nthChildFound = true;
								break;
						}
				}
	
				if (!nthChildFound) {
						error = true;
						break;
				}
	
				const count = childTagCount[currentElement.tagName];
				const tag = currentElement.tagName.toLowerCase();
				const selector = `${tag}:nth-of-type(${count})`;
	
				path.push(selector);
				currentElement = parent;
		}
	
		if (error) {
				console.error(element);
				throw new Error('Unable to create query selector');
		}
	
		path.push('body');
	
		const querySelector = path.reverse().join(' > ');
	
		return querySelector;
	}

	 useEffect(()=>{

		if(clickedEl){

			const target=window.getComputedStyle(clickedEl);
			const cssPropertiesObj={};
			// console.log(target);
			// const marginLeft = parseInt(target.getPropertyValue('margin-left').slice(0,-2));
			

			cssProperties.forEach((property)=>{
				cssPropertiesObj[property] = parseInt( target.getPropertyValue(convertStrtoIfen(property)).slice(0,-2) );
			})
	
			setMarginPadding(cssPropertiesObj);
			// console.log(createQuerySelector(clickedEl));
			
		 }


	 },[clickedEl])


	 const changeInputHandler=(e)=>{
      const {name,value} = e.target

			setElementCss((prevState)=>{
        
				clickedEl.style[name] = `${value}px`
			
				return{
					...prevState,
				elementProperties:{
					...prevState.elementProperties,
					[name]:{
						...prevState.elementProperties[name],
						current: parseInt(value) || 0
					}

				}
	
				}
	
			})


	 }

	 

	 const addDOMstate=()=>{
		const DOMState = localStorageGet('DOMState');

		const localChange ={
			commit_hash: uuidv4(),
			commit_value:[ { changes:[ { } ] , elementRef: ""} ]
		} 

		 cssProperties.forEach((property)=>{
			let current= elementCss.elementProperties[property].current ;
			let prev =elementCss.elementProperties[property].prev ;

			if(current !== prev){
         localChange.commit_value.changes.push({
					[property]: {
						current: current-prev,
						prev: prev

					}
				 })
			}
		 })

		 DOMState.push(localChange)

		 localStorageSet('DOMState',DOMState);

		 console.log(DOMState);

		



	 }


	 if(openInspector){
		return (
			<div className={cx('container')}>
	
				<button onClick={()=>setInspector(!openInspector)}> close</button>
				Testing Inspector

				<div>
					Margin
					<input style={{width:'40px'}} type="text" name="marginTop" value={elementCss.elementProperties.marginTop.current}
					onChange={changeInputHandler}  />
					<input style={{width:'40px'}}type="text" name="marginRight"  value={elementCss.elementProperties.marginRight.current} 
					onChange={changeInputHandler}/>
					<input  style={{width:'40px'}}type="text" name="marginBottom"  value={elementCss.elementProperties.marginBottom.current} 
					onChange={changeInputHandler}/>
					<input  style={{width:'40px'}}type="text" name="marginLeft" value={elementCss.elementProperties.marginLeft.current}
					onChange={changeInputHandler} />
				</div>


				<div>
					Padding
					<input style={{width:'40px'}} type="text" name="paddingTop" value={elementCss.elementProperties.paddingTop.current}
					onChange={changeInputHandler}  />
					<input style={{width:'40px'}}type="text" name="paddingRight"  value={elementCss.elementProperties.paddingRight.current} 
					onChange={changeInputHandler}/>
					<input  style={{width:'40px'}}type="text" name="paddingBottom"  value={elementCss.elementProperties.paddingBottom.current} 
					onChange={changeInputHandler}/>
					<input  style={{width:'40px'}}type="text" name="paddingLeft" value={elementCss.elementProperties.paddingLeft.current}
					onChange={changeInputHandler} />
				</div>

				<div>
					 <button onClick={addDOMstate}>Apply changes</button>
				</div>
			</div>
		)

	 }

	 else return(
		<button style={{position: 'fixed', bottom: '25px',right: '90px'}} 
		onClick={()=>setInspector(!openInspector)}> Open Inspector</button>
	 )

	
}


export default Inspector;