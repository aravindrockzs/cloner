import Inspector from "../components/Inspector";

const  getclonerWebComponentClass=(window,currentElement,reset)=>{


	const template = window.document.createElement('template');
  
	template.innerHTML =
	`
	<div id='inspector-selected' 
	style='border:2px solid  #5056f1; 
	position: fixed;height:0px;box-sizing: border-box;' >
		<div id='image-options' style='display: none'>

			<input type="file" id="upload" accept="image/*">
			<button id="test"> Testing </button>

		</div> 
				
	
	
	</div>



	<div id='inspector-hovered'  style='border:2px dashed #5056f1; position: fixed;
	 pointer-events: none;height:0px;box-sizing: border-box; ' ></div>

	<div id='inspector-double' style='border:2px dashed grey; position: fixed;
	 pointer-events: none;height:0px;box-sizing: border-box;box-shadow:0px 0px 0.5px 20000vh rgba(0, 0, 0, 0.7) '></div>
	
  <div id="canvas-container" style="display: block; box-sizing: border-box; height: 100vh; width:100vw">
		<canvas id="myCanvas" ></canvas>
	
	
	</div>
	
	
	

	`



	return class ClonerShadow extends window.HTMLElement{

			constructor(){
				super();
				this.style="position: fixed; top: 0px; left: 0px;z-index:999999999999;box-sizing: border-box;"
				this.attachShadow({mode:"open"})
				this.shadowRoot.appendChild(template.content.cloneNode(true));
				
			}

			connectedCallback(){

				const fileInput = this.shadowRoot.getElementById('upload');

				// Listen for the change event so we can capture the file
				fileInput.addEventListener('change', (e) => {
						// Get a reference to the file
						const file = e.target.files[0];
		
						// Encode the file using the FileReader API
						const reader = new FileReader();
						reader.onloadend = () => {

							console.log(currentElement.current);

							currentElement.current.src = reader.result;
								// console.log(reader.result);
								// Logs data:<type>;base64,wL2dvYWwgbW9yZ...

								reset()

								const imgOption = this.shadowRoot.getElementById('image-options');
						    imgOption.style.display='none';
								

								
						
						};
						reader.readAsDataURL(file);
						
				});

				


				// for canvas

				const canvas= this.shadowRoot.getElementById('myCanvas');
				canvas.style.width ='100%';
        canvas.style.height='100%';

				canvas.width  = canvas.offsetWidth;
				canvas.height = canvas.offsetHeight;

				const context = canvas.getContext('2d');
				context.lineCap = 'round'
				context.lineJoin = 'round'


				let isDrawing = false;
				let x = 0;
				let y = 0;

				canvas.addEventListener('mousedown',(e)=>{
					x = e.offsetX;
					y = e.offsetY;
					isDrawing = true;

					
				})

				canvas.addEventListener('mousemove', (e) => {
					if (isDrawing) {
						
						drawLine(context, x, y, e.offsetX, e.offsetY);
						x = e.offsetX;
						y = e.offsetY;
					}
				});
				
				canvas.addEventListener('mouseup', (e) => {
					context.clearRect(0, 0, canvas.width, canvas.height);

					if (isDrawing) {
						// drawLine(context, x, y, e.offsetX, e.offsetY);
						context.clearRect(0, 0, canvas.width, canvas.height);
						x = 0;
						y = 0;
						isDrawing = false;
					}
				});
				
				function drawLine(context, x1, y1, x2, y2) {
					context.beginPath();
					context.strokeStyle = 'white';
					context.lineWidth = 3;
					context.moveTo(x1, y1);
					context.lineTo(x2, y2);
					context.stroke();
					context.closePath();
				}

				
				
			}
			

		
			

			render(){
       
				const shadow = this.shadowRoot;

				const x = this.getAttribute('x');
				const y=this.getAttribute('y');

			
				const height = this.getAttribute('height');
				const width = this.getAttribute('width');

				const type = this.getAttribute('type');

			



				

		

					if(type==='click'){
            const double = shadow.getElementById('inspector-double');
						double.style.display = 'none';
					
						const hoveredDiv = shadow.getElementById('inspector-hovered');
					  hoveredDiv.style.display = 'none';

						const clickedDiv = shadow.getElementById('inspector-selected')
						clickedDiv.style.display='block'
						

					

;
						clickedDiv.style.left = `${x}px`;
						clickedDiv.style.top=`${y}px`;
	
						clickedDiv.style.height = `${height}px`;
						clickedDiv.style.width = `${width}px`;
						// clickedDiv.style.borderRadius = '5px';

					}
				
				 

				  if(type==='hover'){

   
						const inspected = shadow.getElementById('inspector-hovered')
						inspected.style.display='block'

						const double = shadow.getElementById('inspector-double');
						double.style.display = 'none';

						const clickedDiv = shadow.getElementById('inspector-selected')
						clickedDiv.style.display='none'
						

						

						inspected.style.left = `${x}px`;
						inspected.style.top=`${y}px`;
						inspected.style.height = `${height}px`;
						inspected.style.width = `${width}px`;
						// inspected.style.borderRadius = '5px';



					}

		      if(type==="initial"){

						const inspected = shadow.getElementById('inspector-hovered')
						inspected.style.display='none'

						const clickedDiv = shadow.getElementById('inspector-selected')
						clickedDiv.style.display='none'

						const double = shadow.getElementById('inspector-double')
						double.style.display='none'
						
					}		
				

					if(type=== 'dblclick'){

						const image = this.getAttribute('image');
            
						const inspected = shadow.getElementById('inspector-hovered')
						inspected.style.display='none'

						const clickedDiv = shadow.getElementById('inspector-selected')
						clickedDiv.style.display='none'
						  
						const double = shadow.getElementById('inspector-double');
						double.style.display = 'block';

						if(image==="true"){

							
							 clickedDiv.style.display="block";
							 double.style.display= 'none';
							 const imgOption = shadow.getElementById('image-options');
						   imgOption.style.display='block';


							clickedDiv.style.left = `${x}px`;
							clickedDiv.style.top=`${y}px`;
							clickedDiv.style.height = `${height}px`;
							clickedDiv.style.width = `${width}px`;
								return;




						}



						

						double.style.left = `${x}px`;
						double.style.top=`${y}px`;
						double.style.height = `${height}px`;
						double.style.width = `${width}px`;
						// double.style.borderRadius = '5px';
					


					}
	

				
				

		


			}


			static get observedAttributes() {
				return ['x','y','height','width','type','image'];
			}

			attributeChangedCallback(name, oldValue, newValue) { // (4)
				this.render();
			}
	 
	}
	
	

}



export default getclonerWebComponentClass;




